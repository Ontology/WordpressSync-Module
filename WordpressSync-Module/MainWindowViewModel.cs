﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using Security_Module;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using WordPress_Module;
using WordPress_Module.Factory;
using WordpressSync_Module.BaseClasses;
using WordpressSync_Module.Factories;
using WordpressSync_Module.Model;

namespace WordpressSync_Module
{
    public class MainWindowViewModel : ViewModelBase
    {
        private clsLocalConfig localConfig;
        private WordPressOntologyEngine wordPressOntologyEngine;
        private clsAuthenticate authencitation;
        
        
        private string userName;
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_UserName);
            }
        }

        private string baseUrl;
        public string BaseUrl
        {
            get { return baseUrl; }
            set
            {
                baseUrl = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_BaseUrl);
            }
        }

        private Uri BaseUri;

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_Password);
            }
        }

        private BlogQueryInfoValidationResult lastQueryInfoValidationResult;
        public BlogQueryInfoValidationResult LastQueryInfoValidationResult
        {
            get { return lastQueryInfoValidationResult; }
            set
            {
                lastQueryInfoValidationResult = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LastQueryInfoValidationResult);
            }
        }

        private PostListInfoValidationResult lastPostListInfoValidationResult;
        public PostListInfoValidationResult LastPostListInfoValidationResult
        {
            get { return lastPostListInfoValidationResult; }
            set
            {
                lastPostListInfoValidationResult = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LastPostListInfoValidationResult);
            }
        }

        private List<UserBlog> userBlogs = null;
        public List<UserBlog> UserBlogs
        {
            get { return userBlogs; }
            set
            {
                userBlogs = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_UserBlogs);
            }
        }

        private List<WebServiceItem> webServices = null;
        public List<WebServiceItem> WebServices
        {
            get { return webServices; }
            set
            {
                webServices = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_WebServices);
            }
        }

        private WebServiceItem selectedWebService;
        public WebServiceItem SelectedWebService
        {
            get
            {
                return selectedWebService;
            }
            set
            {
                selectedWebService = value;
                if (selectedWebService != null)
                {
                    BaseUrl = selectedWebService.UrlItem != null ? selectedWebService.UrlItem.Name : "";
                    UserName = selectedWebService.UserItem != null ? selectedWebService.UserItem.Name : "";
                    Password = selectedWebService.Password;
                }
                    

                RaisePropertyChanged(NotifyChanges.MainWindow_SelectedWebServices);
            }
        }

        private UserBlog selectedUserBlog;
        public UserBlog SelectedUserBlog
        {
            get
            {
                return selectedUserBlog;
            }
            set
            {
                selectedUserBlog = value;
                ValidateActionSource();
                RaisePropertyChanged(NotifyChanges.MainWindow_SelectedUserBlog);
            }
        }

        private void ValidateActionSource()
        {
            LastQueryInfoValidationResult = ValidationEngine.IsOk_BlogQueryInfo(userName, password, baseUrl);
            IsEnabled_GetBlogs = LastQueryInfoValidationResult == BlogQueryInfoValidationResult.None;
            QueryInfoErrorColor = LastQueryInfoValidationResult == BlogQueryInfoValidationResult.None ? Brushes.Transparent : Brushes.Red;
            LastPostListInfoValidationResult = ValidationEngine.IsOk_PostListInfo(UserName, password, baseUrl, selectedUserBlog);
            IsEnabled_PostList = LastPostListInfoValidationResult == PostListInfoValidationResult.None;
        }

        private bool isEnabled_GetBlogs;
        public bool IsEnabled_GetBlogs
        {
            get
            {
                return isEnabled_GetBlogs;
            }
            set
            {
                isEnabled_GetBlogs = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_GetBlogs);
            }
        }

        private bool isEnabled_PostsList;
        public bool IsEnabled_PostList
        {
            get
            {
                return isEnabled_PostsList;
            }
            set
            {
                isEnabled_PostsList = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_PostList);

            }
        }

        private Brush queryInfoErrorColor;
        public Brush QueryInfoErrorColor
        {
            get { return queryInfoErrorColor; }
            set
            {
                queryInfoErrorColor = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_QueryInfoErrorColor);
            }
        }

        private ObservableCollection<GridPostItem> postList;
        public ObservableCollection<GridPostItem> PostList
        {
            get { return postList; }
            set
            {
                postList = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_PostList);
            }

        }

        public void GetPostList()
        {
            
            if (SelectedUserBlog != null)
            {
                var result = wordPressOntologyEngine.GetPostList(SelectedUserBlog, BaseUrl, UserName, Password, true);

                
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    PostList = new ObservableCollection<GridPostItem>(wordPressOntologyEngine.PostList.Select(postItem => new GridPostItem(postItem)));
                }
                
                
            }
            
        }

        public MainWindowViewModel()
        {
            localConfig = new clsLocalConfig(new Globals());
            Initialize();
        }

        private void Initialize()
        {
            authencitation = new clsAuthenticate(localConfig.Globals);
            var result = authencitation.Authenticate(true, false, frmAuthenticate.ERelateMode.NoRelate, true);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                localConfig.SecurityWork = new clsSecurityWork(localConfig.Globals);
                result = localConfig.SecurityWork.initialize_User(authencitation.objOItem_User);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    localConfig.UserItem = authencitation.objOItem_User;
                    wordPressOntologyEngine = new WordPressOntologyEngine(localConfig.Globals);
                    WebServiceFactory webServiceFactory = new WebServiceFactory(localConfig);
                    result = webServiceFactory.GetData_BaseConfig();
                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        WebServices = webServiceFactory.WebServices;
                    }
                }
                
            }
            else
            {
                Environment.Exit(-1);
            }
            

        }

        public void GetUserBlogs()
        {
            var result = wordPressOntologyEngine.GetUserBlogs(BaseUrl, UserName, Password, true);
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                UserBlogs = wordPressOntologyEngine.UserBlogs;
            }
        }
    }

}
